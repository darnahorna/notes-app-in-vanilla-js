# Notes App Vanilla JS 🗒

This application is part of the test task for internship in Radenсy company. 

The code for this makes use of the following HTML, CSS, JS features: Local Storage, ES6 Import/Export Syntax, Classes, Arrow Functions, Static Methods, Callback Functions, Event Listeners, QuerySelector, GetElementById, Template Strings, Class List, CSS Psuedo-Classes, Flexbox and HTML5 Data Attributes.

## Task

1. Your task is to create a notes app in JS as a web app. Users can add, edit and remove notes. 

2. List of notes is displayed in a form of table (HTML representation may vary: table, divs etc). The columns are time of creation, note content, note category. Categories are predefined: “Task”, “Random Thought”, “Idea”.

3. Notes in the table should also display a list of dates mentioned in this note as a separate column. For example, for a note “I’m gonna have a dentist appointment on the 3/5/2021, I moved it from 5/5/2021” the dates column is “3/5/2021, 5/5/2021”.

3. Users can archive notes. Archived notes are not shown in the notes list. Users can view archived notes and unarchive them.

4. There should also be a summary table which counts notes by categories: separately for active and archived. The table is updated whenever users perform some action on notes. The summary table is shown on the same page as the notes table.

5. There is no need to implement data storage. Simply create a JS variable which stores the data and prepopulate it with 7 notes so that they are shown when the page is reloaded.



## Usage
To use/test application, just click here - [ Notes App Vanilla JS](https://note-app-vanilla-js.netlify.app/).


## Getting Started
These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

1. Before you begin, choose the folder you want to work in. 
2. Clone a repository. The files from the remote repository will be downloaded to your computer.
```
clone git@gitlab.com:darnahorna/notes-app-in-vanilla-js.git
```
3. Now you can open the folder in any of IDE for editing code.

## Author

Daria Nahorna 

## Acknowledgments
This project is designed for educational purposes. The project developed the layout, design and functionality of the application. 
