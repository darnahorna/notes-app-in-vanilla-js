import NotesAPI from "./NotesAPI.js";
import NotesView from "./NotesView.js";

export default class App {
  constructor(root) {
    localStorage.setItem("notesapp-notes", JSON.stringify(this.testData));
    this.notes = [];
    this.archive = [];
    this.stats = [];
    this.activeNote = null;
    this.view = new NotesView(root, this._handlers());
    this.view.updateNotePreviewVisibility(false);
    this._refreshActiveNotes();
    this._refreshArchiveNotes();
    this._refreshStats();
  }

  _selectNoteById(noteId) {
    const noteToEdit = NotesAPI.getNoteById(noteId);
    this._setActiveNote(noteToEdit);
  }

  _refreshActiveNotes() {
    const notes = NotesAPI.getAllNotes();
    this._setNotes(notes); //updates a list of visible notes

    if (notes.length > 0) {
      this._setActiveNote(notes[0]);
    }
  }
  _refreshArchiveNotes() {
    const archive = NotesAPI.getAllFromArchive();

    //console.log(notes);
    this._setArchive(archive); //updates a list of visible notes
  }

  _refreshStats() {
    const archive = NotesAPI.getAllFromArchive();
    const notes = NotesAPI.getAllNotes();
    const stats = NotesAPI.getAmount(notes, archive);

    this._setStats(stats);
  }

  _setActiveNote(note) {
    this.activeNote = note;
    this.view.updateActiveNote(note); //update text in form
  }

  _setNotes(notes) {
    //keep a reference to the count list of notes
    this.notes = notes;
    this.view.updateNoteList(notes);
  }

  _setArchive(archive) {
    this.archive = archive;
    this.view.updateArchiveList(archive);
  }

  _setStats(stats) {
    this.stats = stats;
    this.view.updateStatsList(stats);
  }

  _handlers() {
    return {
      onNoteAdd: () => {
        const newNote = {
          name: "New note",
          category: "Task",
          content: "Take a note",
        };
        NotesAPI.saveNote(newNote);
        this._setActiveNote(newNote);
        this._refreshActiveNotes();
        this._refreshStats();
      },

      onNoteDelete: (noteId) => {
        NotesAPI.deleteNote(noteId);
        this._refreshActiveNotes();
        this._refreshStats();
      },
      onNoteArchive: (noteId) => {
        NotesAPI.archiveNote(noteId);
        this._refreshActiveNotes();
        this._refreshArchiveNotes();
        this._refreshStats();
      },
      onNoteEdit: (noteId) => {
        this._selectNoteById(noteId);
      },
      onNoteSave: (name, category, content) => {
        const editedNote = {
          id: this.activeNote.id,
          name: name,
          category: category,
          content: content,
        };
        NotesAPI.saveNote(editedNote);
        this._refreshActiveNotes();
        this._refreshStats();
      },
      onUnArchive: (noteId) => {
        NotesAPI.unArchiveNote(noteId);
        this._refreshArchiveNotes();
        this._refreshActiveNotes();
        this._refreshStats();
      },

      onNoteSelect: (noteId) => {
        const selectedNote = this.notes.find((note) => note.id == noteId);
        this._setActiveNote(selectedNote);
      },
    };
  }
  testData = [
    {
      id: 763747,
      name: "Shopping List",
      created: "12 Sep 2022",
      category: "Task",
      content: "Tomatoes, bread, cheese",
      dates: "",
    },
    {
      id: 9290,
      name: "The theory of evolution",
      created: "15 Sep 2022",
      category: "Random Thoughts",
      content:
        "Evolution is the process of change in all forms of life over generations, and evolutionary biology is the study of how evolution occurs.",
      dates: "",
    },
    {
      id: 15590,
      name: "New Features",
      created: "10 Sep 2022",
      category: "Idea",
      content: "Maybe I need to go to dentist 03/04/2022 and 05/04/2022 ",
      dates: "03/04/2022, 05/04/2022",
    },
    {
      id: 8582,
      name: "William Gaddis",
      created: "7 Sep 2022",
      category: "Idea",
      content:
        "William Thomas Gaddis, Jr. was an American novelist. The first and longest of his five novels, The Recognitions, was named one of TIME magazine's 100 best novels from 1923 to 2005 and two others, J R and A Frolic of His Own, won the annual U.S. National Book Award for Fiction",
      dates: "",
    },
    {
      id: 3755,
      name: "Books",
      created: "5 Sep 2022",
      category: "Task",
      content:
        "The Lean Startup: How Today's Entrepreneurs Use Continuous Innovation to Create Radically Successful Businesses is a book by Eric Ries describing his proposed lean startup strategy for startup companies. Ries developed the idea for the lean startup from his experiences as a startup advisor, employee, and founder.",
      dates: "",
    },
    {
      id: 46400,
      name: "I so love my cat",
      created: "10 Sep 2022",
      category: "Random Thoughts",
      content:
        "Research has proven that petting a cat causes a release of the “love hormone” in humans. The technical term for this neurochemical is oxytocin, a hormone that is released when people fall in love.",
      dates: "",
    },
    {
      id: 46700,
      name: "My Name Is",
      created: "11 Sep 2022",
      category: "Random Thoughts",
      content:
        "My Name Is is a song by American rapper Eminem from his second album The Slim Shady LP (1999). It is also the opening song and lead single of the album.",
      dates: "",
    },
  ];
}
