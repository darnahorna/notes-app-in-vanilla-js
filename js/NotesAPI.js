export default class NotesAPI {
  //class for Note
  static getAllNotes() {
    //method to get all notes
    const notes = JSON.parse(localStorage.getItem("notesapp-notes") || "[]"); //from local storage key get list of notes or empty array
    return notes.sort((a, b) => {
      return new Date(a.created) > new Date(b.created) ? -1 : 1; //sort notes by time of creation
    });
  }

  static getNoteById(noteId) {
    //for selecting note in a list
    const notes = NotesAPI.getAllNotes(); //from all notes
    const noteById = notes.find((note) => note.id == noteId); //looking for the note with same id
    if (noteById) {
      return noteById;
    } else {
      return "Sorry note with this id is not found =(";
    }
  }

  static getAmount(notes, archive) {
    const statsNotes = NotesAPI.getStats(notes);
    const statsArch = NotesAPI.getStats(archive);

    const transformedA = Object.entries(statsNotes).map(
      //
      ([category, active]) => {
        return { category: category, active: active };
      }
    );
    const transformedB = Object.entries(statsArch).map(
      ([category, archived]) => {
        return { category: category, archived: archived };
      }
    );

    const merge = (...arrays) => {
      //merge final result
      const merged = {};

      arrays.forEach((item) =>
        item.forEach((innerItem) =>
          Object.assign((merged[innerItem.category] ??= {}), innerItem)
        )
      );

      return Object.values(merged);
    };
    const result = merge(transformedA, transformedB);

    return result;
  }

  static getStats(notes) {
    const result = notes.reduce((acc, cur) => {
      acc[cur.category] = (acc[cur.category] || 0) + 1; //if category already exist id or its value 0
      return acc;
    }, {});
    const obj = Object.assign({}, result);
    return obj;
  }

  static getAllFromArchive() {
    const archiveNotes = JSON.parse(
      localStorage.getItem("notesapp-archive-notes") || "[]"
    ); //from local storage key get list of notes or empty array
    return archiveNotes.sort((a, b) => {
      return a.category > b.category ? -1 : 1; //sort notes by time of creation
    });
  }

  static saveNote(noteToSave) {
    //save a new note method
    const notes = NotesAPI.getAllNotes(); //from all notes
    const existing = notes.find((note) => note.id == noteToSave.id); //looking for the note with same id

    if (existing) {
      //if this note exist
      existing.name = noteToSave.name; //update fields one by one
      existing.category = noteToSave.category;
      existing.content = noteToSave.content;
      existing.dates = NotesAPI.parseDate(noteToSave.content);
    } else {
      //if not
      noteToSave.dates = NotesAPI.parseDate(noteToSave.content);
      noteToSave.id = Math.floor(Math.random() * 10000); //create new note with a new id and creation date
      noteToSave.created = NotesAPI.setDate();
      notes.push(noteToSave); //push this note to the rest of the notes
    }

    localStorage.setItem("notesapp-notes", JSON.stringify(notes)); //set new list of notes to local storage
  }

  static deleteNote(idNoteToDelete) {
    const notes = NotesAPI.getAllNotes(); //from all notes
    const newNotes = notes.filter((note) => note.id !== idNoteToDelete); //filter all notes apart from the note with the same id

    localStorage.setItem("notesapp-notes", JSON.stringify(newNotes)); //set new list of notes (without old id) to local storage
  }

  static archiveNote(idNoteToArchive) {
    //archive a note method
    const notes = NotesAPI.getAllNotes(); //from all notes
    const archiveNotes = NotesAPI.getAllFromArchive();
    const archive = notes.find((note) => note.id == idNoteToArchive); //looking for the note with same id

    archiveNotes.push(archive);
    const index = notes.indexOf(archive);
    if (index > -1) {
      // only splice array when item is found
      notes.splice(index, 1); // 2nd parameter means remove one item only
    }

    localStorage.setItem("notesapp-notes", JSON.stringify(notes)); //set new list of notes to local storage
    localStorage.setItem(
      "notesapp-archive-notes",
      JSON.stringify(archiveNotes)
    ); //set new list of notes to local storage
  }
  static unArchiveNote(idNoteToArchive) {
    //archive a note method

    const archiveNotes = NotesAPI.getAllFromArchive();
    const notes = NotesAPI.getAllNotes(); //from all notes

    const noteToMove = archiveNotes.find((note) => note.id == idNoteToArchive); //looking for the note with same id

    notes.push(noteToMove);
    const index = archiveNotes.indexOf(noteToMove);
    if (index > -1) {
      // only splice array when item is found
      archiveNotes.splice(index, 1); // 2nd parameter means remove one item only
    }

    //push this note to the rest of the notes
    localStorage.setItem("notesapp-notes", JSON.stringify(notes)); //set new list of notes to local storage
    localStorage.setItem(
      "notesapp-archive-notes",
      JSON.stringify(archiveNotes)
    ); //set new list of notes to local storage
  }

  static setDate() {
    let month = [
      "Jan",
      "Feb",
      "Mar",
      "Apr",
      "May",
      "Jun",
      "Jul",
      "Aug",
      "Sep",
      "Oct",
      "Nov",
      "Dec",
    ];

    let now = new Date();

    let currentDate = now.getDate();
    let currentMonth = month[now.getMonth()]; //using index of array
    let currentYear = now.getFullYear();

    return `${currentDate} ${currentMonth} ${currentYear}`;
  }
  static parseDate(str) {
    const m = str.match(/\d{2}([\/.-])\d{2}\1\d{4}/g);
    return m ? m : "";
  }
  static ifSpace(value) {
    return value.indexOf(" ") >= 0 ? value.replace(/\s/g, "") : value;
  }
}
