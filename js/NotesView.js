export default class NotesView {
  //class for rendering data
  constructor(
    root,
    {
      onNoteSelect,
      onNoteAdd,
      onNoteEdit,
      onNoteSave,
      onNoteDelete,
      onNoteArchive,
      onUnArchive,
    } = {}
  ) {
    //initialization of all functions
    this.root = root;
    this.onNoteSelect = onNoteSelect;
    this.onNoteAdd = onNoteAdd;
    this.onNoteEdit = onNoteEdit;
    this.onNoteDelete = onNoteDelete;
    this.onNoteArchive = onNoteArchive;
    this.onNoteSave = onNoteSave;
    this.onUnArchive = onUnArchive;

    this.root.innerHTML = `
    
<div class ="text-center">All notes</div>
    <div class="content-table">
       <div class="content-header table d-flex justify-space-between">
          <div class="flex-row">Name</div>
          <div class="flex-row">Created</div>
          <div class="flex-row">Category</div>
          <div class="flex-row">Content</div>
          <div class="flex-row">Dates</div>
          <div class="flex-row">
            <i class="fa-solid fa-box-archive"></i>
            <i class="fa-solid fa-trash"></i>
          </div>
          </div>
      <div class="content-note"> </div> 
    </div>
    <div class="mb-4">
      <button class="button new-note-btn mb-2 mt-2 me-2" type="submit"> Create Note
      </button>
     

      <div class="notes-preview mb-4">
        <input class="notes-title" type="text" placeholder="New Note" />
        <select name="category" class="category" required>
          <option value="">Choose category</option>
          <option value="Idea">Idea</option>
          <option value="Random Thoughts">Random Thoughts</option>
          <option value="Task">Task</option>
        </select>
        <textarea class="notes-body" rows="3">Take note..</textarea>
        <div>
          <button class="button save-note-btn" type="submit">Save</button>
        </div>
      </div>
 <div class ="text-center">Archive</div>
 <div class="content-table mb-4">
       <div class="content-header table d-flex justify-space-between">
          <div class="flex-row">Name</div>
          <div class="flex-row">Created</div>
          <div class="flex-row">Category</div>
          <div class="flex-row">Content</div>
          <div class="flex-row">Dates</div>
          <div class="flex-row">
            <i class="fa-solid fa-box-archive"></i>
            <i class="fa-solid fa-trash"></i>
          </div>
          </div>
     <div class="archive-content-note"> </div> 
    </div>

 <div class ="text-center">Stats</div>
     <div class="content-table">
        <div class="content-header table d-flex justify-space-between">
          <div class="flex-row-archived">Note Category</div>
          <div class="flex-row-archived">Active</div>
          <div class="flex-row-archived">Archived</div>
        </div>
        <div class = "content-note-stats"></div>
        
      </div>


    </div> 
    `;

    const btnAddNote = this.root.querySelector(".new-note-btn");
    //const btnSeeArchive = this.root.querySelector(".see-archive-btn");
    //const btnSeeStats = this.root.querySelector(".see-stats-btn");
    const inputName = this.root.querySelector(".notes-title");
    const inputCategory = this.root.querySelector(".category");
    const inputContent = this.root.querySelector(".notes-body");
    const btnSaveNote = this.root.querySelector(".save-note-btn");

    btnAddNote.addEventListener("click", () => {
      this.updateNotePreviewVisibility(true);
      this.onNoteAdd();
    });

    btnSaveNote.addEventListener("click", () => {
      const updatedName = inputName.value.trim();
      const updatedCategory = inputCategory.value.trim();
      const updatedContent = inputContent.value.trim();

      this.onNoteSave(updatedName, updatedCategory, updatedContent);
      this.updateNotePreviewVisibility(false);
    });
  }

  _createListItemHTML(id, name, created, category, content, dates) {
    //function for rendering note item

    const MAX_CONTENT_LENGTH = 30;
    const MAX_NAME_LENGTH = 20;
    return `
    <div class="content-note-item d-flex justify-space-between" data-note-id="${id}">
        <div class="flex-row note-name" >
              <i class="note-icon fa-solid fa-${
                category === "Idea"
                  ? "brain"
                  : category === "Task"
                  ? "list-check"
                  : category === "Random Thoughts"
                  ? "comment"
                  : "folder-open"
              }"></i> 
             ${name.substring(0, MAX_NAME_LENGTH)}
                    ${name.length > MAX_NAME_LENGTH ? "..." : ""}
        </div>
        <div class="flex-row note-created">
           ${created.toLocaleString(undefined, {
             dateStyle: "full",
             timeStyle: "short",
           })}
        </div>
        <div class="flex-row note-category">${category}
        </div>
        <div class="flex-row note-content">
          ${content.substring(0, MAX_CONTENT_LENGTH)}
          ${content.length > MAX_CONTENT_LENGTH ? "..." : ""}
        </div>
        <div class="flex-row note-dates wrap">${dates}</div>
        <div class="flex-row note-actions">
              <i class="action-link fa-solid fa-pen-to-square" title = "edit"></i>
              <i class="action-link fa-solid fa-box-archive" title = "archive"></i
              ><i class="action-link fa-solid fa-trash" title = "delete"></i>
        </div>
    </div>
          `;
  }

  _createArchiveItemHTML(id, name, created, category, content, dates) {
    //function for rendering note item

    const MAX_CONTENT_LENGTH = 30;
    const MAX_NAME_LENGTH = 20;
    return `
    <div class="content-archive-item d-flex justify-space-between" data-archive-id="${id}">
        <div class="flex-row note-name" >
              <i class="note-icon fa-solid fa-${
                category === "Idea"
                  ? "brain"
                  : category === "Task"
                  ? "list-check"
                  : category === "Random Thoughts"
                  ? "comment"
                  : "folder-open"
              }"></i> 
             ${name.substring(0, MAX_NAME_LENGTH)}
                    ${name.length > MAX_NAME_LENGTH ? "..." : ""}
        </div>
        <div class="flex-row note-created">
           ${created.toLocaleString(undefined, {
             dateStyle: "full",
             timeStyle: "short",
           })}
        </div>
        <div class="flex-row note-category">${category}
        </div>
        <div class="flex-row note-content">
          ${content.substring(0, MAX_CONTENT_LENGTH)}
                    ${content.length > MAX_CONTENT_LENGTH ? "..." : ""}
        </div>
        <div class="flex-row note-dates"> ${dates}</div>
        <div class="flex-row note-actions">
              <i class="action-link fa-solid fa-boxes-packing" title = "unarchive"></i>
        </div>
    </div>
          `;
  }

  _createStatsItemHTML(category, active, archived) {
    return `
    <div class=" content-item-stats d-flex justify-space-between">
          <div class="flex-row-archived">
            <i class="note-icon fa-solid fa-${
              category === "Idea"
                ? "brain"
                : category === "Task"
                ? "list-check"
                : category === "Random Thoughts"
                ? "comment"
                : "folder-open"
            }"></i> ${category}
          </div>
          <div class="flex-row-archived">${active ? active : 0}</div>
          <div class="flex-row-archived">${archived ? archived : 0}</div>
        </div>
          `;
  }

  updateNoteList(notes) {
    //update every item in content table
    const notesListContainer = this.root.querySelector(".content-note");

    notesListContainer.innerHTML = "";

    for (const note of notes) {
      const html = this._createListItemHTML(
        note.id,
        note.name,
        note.created,
        note.category,
        note.content,
        note.dates
      );

      notesListContainer.insertAdjacentHTML("beforeend", html);
    }

    notesListContainer
      .querySelectorAll(".content-note-item")
      .forEach((noteListItem) => {
        const btnDeleteNote = noteListItem.querySelector(".fa-trash");
        btnDeleteNote.addEventListener("click", () => {
          const doDelete = confirm(
            "Are you sure you want to delete this note?"
          );

          if (doDelete) {
            this.onNoteDelete(parseInt(noteListItem.dataset.noteId));
          }
        });

        noteListItem
          .querySelector(".fa-box-archive")
          .addEventListener("click", () => {
            const doArchive = confirm(
              "Are you sure you want to archive this note?"
            );
            if (doArchive) {
              this.onNoteArchive(parseInt(noteListItem.dataset.noteId));
              // console.log(`archived id is ${noteListItem.dataset.noteId}`);
            }
          });
        const btnEditNote = noteListItem.querySelector(".fa-pen-to-square");

        btnEditNote.addEventListener("click", () => {
          this.updateNotePreviewVisibility(true);
          this.onNoteEdit(noteListItem.dataset.noteId);
          //console.log(noteListItem);
        });
      });
  }

  updateArchiveList(notes) {
    //update every item in content table
    const notesListContainer = this.root.querySelector(".archive-content-note");

    notesListContainer.innerHTML = "";

    for (const note of notes) {
      const html = this._createArchiveItemHTML(
        note.id,
        note.name,
        note.created,
        note.category,
        note.content,
        note.dates
      );

      notesListContainer.insertAdjacentHTML("beforeend", html);
    }

    notesListContainer
      .querySelectorAll(".content-archive-item")
      .forEach((noteListItem) => {
        const btnDeleteNote = noteListItem.querySelector(".fa-boxes-packing");
        btnDeleteNote.addEventListener("click", () => {
          const doUnArchive = confirm(
            "Are you sure you want to unarchive this note?"
          );

          if (doUnArchive) {
            this.onUnArchive(parseInt(noteListItem.dataset.archiveId));
            //console.log(noteListItem.dataset);
          }
        });
      });
  }

  updateStatsList(stats) {
    //update every item in stats table
    const notesListContainer = this.root.querySelector(".content-note-stats");

    notesListContainer.innerHTML = "";

    for (const stat of stats) {
      const html = this._createStatsItemHTML(
        stat.category,
        stat.active,
        stat.archived
      );

      notesListContainer.insertAdjacentHTML("beforeend", html);
    }
  }

  updateActiveNote(note) {
    //set values to text fields(whe edit)
    this.root.querySelector(".notes-title").value = note.name;
    this.root.querySelector(".category").value = note.category;
    this.root.querySelector(".notes-body").value = note.content;

    this.root.querySelectorAll(".content-note-item").forEach((noteListItem) => {
      noteListItem.classList.remove("content-note-item-selected");
    });
  }

  updateNotePreviewVisibility(isNeeded) {
    //show/hide preview form
    if (isNeeded) {
      this.root.querySelector(".notes-preview").classList.add("d-block");
      this.root.querySelector(".notes-preview").classList.remove("d-none");
    } else {
      this.root.querySelector(".notes-preview").classList.add("d-none");
      this.root.querySelector(".notes-preview").classList.remove("d-block");
    }
  }
}
